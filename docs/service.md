# Services & Resources

Here is the main model for service composition, also included in the Gaia-X Architecture document.

A `Service Offering` can be associated with other `Service Offerings`.

```mermaid
classDiagram
    ServiceOffering o-- Resource: aggregationOf
    Resource o-- Resource: aggregationOf

class Resource{
    <<abstract>>
}

    Resource <|-- "subClassOf" PhysicalResource: 
    Resource <|-- "subClassOf" VirtualResource
    VirtualResource <|-- "subClassOf" InstantiatedVirtualResource

    ServiceOffering "1" ..> "1" Participant : providedBy

    PhysicalResource "1" ..> "0..*" Participant : ownedBy
    PhysicalResource "1" ..> "0..*" Participant : manufacturedBy
    PhysicalResource "1" ..> "1..*" Participant : maintainedBy

    VirtualResource "1" ..> "1..*" Participant : copyrightOwnedBy

    InstantiatedVirtualResource <..> ServiceInstance : equivalent to
    InstantiatedVirtualResource "1" ..> "1..*" Participant : tenantOwnedBy
    InstantiatedVirtualResource "1" ..> "1..*" Participant : maintainedBy
``` 

## Service offering

This is the generic format for all service offerings

| Version | Attribute              | Card. | Signed by | Comment                                         |
|---------|------------------------|-------|-----------|-------------------------------------------------|
| 1.0     | `providedBy`           | 1     | State     | a resolvable link to the `participant` self-description providing the service    |
| 1.0     | `aggregationOf[]`      | 0..*  | State     | a resolvable link to the `resources` self-description related to the service and that can exist independently of it. |
| 1.0     | `termsAndConditions[]` | 1..*  | State     | a resolvable link to the Terms and Conditions appling to that service. |
| 1.1     | `policies[]`           | 0..*  | State     | a list of `policy` expressed using a DSL (e.g., Rego or ODRL) |
| 1.x     | `gdpr`                 | 0..1  |*see below*| Specific attributes for the General Data Protection Regulation. |
| 1.x     | `lgpd`                 | 0..1  |*see below*| Specific attributes for the General Personal Data Protection Law. (_Lei Geral de Proteção de Dados Pessoais_) |
| 1.x     | `pdpa`                 | 0..1  |*see below*| Specific attributes for the Personal Data Protection Act 2012. |


**TermsAndConditions structure**
| Version | Attribute              | Card. | Signed by | Comment                                         |
|---------|------------------------|-------|-----------|-------------------------------------------------|
| 1.0     | `URL`                  | 1     | State     | a resolvable link to document    |
| 1.0     | `hash`                 | 1     | State     | sha256 hash of the above document. |


**Consistency rules**

**TO BE REPHRASED - START**
- `gdpr` attributes are mandatory for `provider` legally located in EEA or providing goods or services in EEA.
- `lgpd` attributes are mandatory for `provider` legally located in Brazil or providing goods or services in Brazil.
- `pdpa` attributes are mandatory for `provider` legally located in Singapore or providing goods or services in Singapore.

**TO BE REPHRASED - END**

### GDPR

| Version | Attribute                 | Card. | Signed by | Comment                                 |
|---------|---------------------------|-------|-----------|-----------------------------------------|
| x.x     | `to_be_defined`           | 1     | State, EDPB CoC | mandatory public information as defined in [GDPR art 13](https://gdpr-info.eu/art-13-gdpr/) |
| x.x     | `to_be_defined`           | 1     | State, EDPB CoC | mandatory public information as defined in [GDPR art 14](https://gdpr-info.eu/art-14-gdpr/) |


### LDPR

| Version | Attribute                 | Card. | Signed by | Comment                                 |
|---------|---------------------------|-------|-----------|-----------------------------------------|
| x.x     | `to_be_defined`           | 1     | to be defined | mandatory public information as defined in LDPR |

### PDPA

| Version | Attribute                 | Card. | Signed by | Comment                                 |
|---------|---------------------------|-------|-----------|-----------------------------------------|
| x.x     | `to_be_defined`           | 1     | to be defined | mandatory public information as defined in PDPA |

Addition specific criteria per Service Offering are described in the next section.

## Resource

A resource aggregates with Service Offering.

| Version | Attribute               | Card. | Signed by | Comment                                         |
|---------|-------------------------|-------|-----------|-------------------------------------------------|
| 1.0     | `aggregationOf[]`       | 0..*  | State     | `resources` related to the resource and that can exist independently of it. |

### Physical Resource

A Physical Resouce inherits from a Resource.  
A Physical resource is, and not limited to, a datacenter, a baremetal service, a warehouse, a plant. Those are entities that have a weigth and position in our space.

| Version | Attribute              | Card. | Signed by | Comment                                     |
|---------|------------------------|-------|-----------|---------------------------------------------|
| 1.0     | `maintainedBy[]`       | 1..*  | State     | a list of `participant` maintaining the resource in operational condition. |
| 1.0     | `ownedBy[]`            | 0..*  | State     | a list of `participant` owning the resource. |
| 1.0     | `manufacturedBy[]`     | 0..*  | State     | a list of `participant` manufacturing the resource. |
| 1.0     | `locationAddress[].country` | 1..*  | State     | a list of physical location in ISO 3166-1 alpha2, alpha-3 or numeric format. |
| 1.0     | `location[].gps`       | 0..*  | State     | a list of physical GPS in [ISO 6709:2008/Cor 1:2009](https://en.wikipedia.org/wiki/ISO_6709) format. |

### Virtual Resource

A Virtual Resource inherits from a Resource.  
A Virtual resource is a resource describing recorded information such as, and not limited to, a dataset, a software, a configuration file, an AI model.

| Version | Attribute            | Card. | Signed by | Comment                                   |
|---------|----------------------|-------|-----------|-------------------------------------------|
| 1.0     | `copyrightOwnedBy[]` | 1..*  | State     | a list of copyright owner either as a free form string or `participant` self-description |
| 1.0     | `license[]`          | 1..*  | State     | a list of [SPDX](https://github.com/spdx/license-list-data/tree/master/jsonld) license identifiers or URL to license document |

### Instantiated Virtual Resource

An Instantiated Virtual Resource inherits from a Virtual Resource.  
An Instantiated Virtual resource is a running resource exposing endpoints such as, and not limited to, a running process, an online API, a network connection, a virtual machine, a container, an operating system.

| Version | Attribute            | Card. | Signed by | Comment                                   |
|---------|----------------------|-------|-----------|-------------------------------------------|
| 1.0     | `maintainedBy[]`     | 1..*  | State     | a list of `participant` maintaining the resource in operational condition. |
| 1.0     | `tenantOwnedBy[]`    | 1..*  | State     | a list of `participant` with contractual relation with the resource. |
| 1.x     | `virtualLocation[]`  | 1..*  | State     | a list of location such an Availability Zone, network segment, IP range, AS number, ... (format to be specified in a separated table) |
| 1.x     | `endpoint[]`         | 1..*  | State     | a list of exposed endpoints as defined in [ISO/IEC TR 23188:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:tr:23188:ed-1:v1:en:term:3.1.7) |
