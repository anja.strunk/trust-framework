# Participant

```mermaid
classDiagram

class Participant {
    <<abstract>>
}

Participant <|-- LegalPerson
Participant <|-- NaturalPerson
```

<!--
TODO: JUNE 2022
flowchart TD

prov --\> part
cons --\> part
part --\> legal
part --\> natural

prov[Provider]
cons[Consumer]
part[Participant]
legal[Legal Person]
natural[Natural Person]
-->

## Legal person

For legal person ,such as and not limited to organisation, business entity, the attributes are

| Version | Attribute          | Cardinality | Signed by | Comment                                           |
|---------|--------------------|-------|-----------|---------------------------------------------------|
| 1.0     | [`registrationNumber`](https://schema.org/taxID) | 1     | State | Country's registration number which identify one specific company. |
| 1.0     | `headquarterAddress`.[`country`](https://schema.org/addressCountry) | 1     | State | Physical location in [ISO 3166-1](https://www.iso.org/iso-3166-country-codes.html) alpha2, alpha-3 or numeric format. |
| 1.0     | `legalAddress`.[`country`](https://schema.org/addressCountry) | 1     | State | location of legal registration in [ISO 3166-1](https://www.iso.org/iso-3166-country-codes.html) alpha2, alpha-3 or numeric format. |
| 1.0     | [`leiCode`](https://schema.org/leiCode)   | 0..1 | gleif | Unique LEI number as defined by <https://www.gleif.org>. |
| 1.0     | [`parentOrganisation[]`](https://schema.org/parentOrganization)   | 0..* | State | a list of direct `participant` that this entity is a subOrganization of, if any. |
| 1.0     | [`subOrganisation[]`](https://schema.org/subOrganization)   | 0..* | State | a list of direct `participant` with an legal mandate on this entity, e.g., as a subsidiary. |

**Consistency rules**

- If `legalAddress.country` is located in [European Economic Area](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Glossary:European_Economic_Area_(EEA)), Iceland, Lichtenstein and Norway then `registrationNumber` must be a valid ISO 6523 EUID as specified in the section 8 of the Commission Implementing [Regulation (EU) 2015/884](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32015R0884).  
This number can be found via the [EU Business registers portal](https://e-justice.europa.eu/content_find_a_company-489-en.do)
- If `legalAddress.country` is located in [United States of America](https://en.wikipedia.org/wiki/United_States), than a valid `legalAddress.state` using the [two–letter state abbreviations](https://pe.usps.com/text/pub28/28apb.htm) is mandatory
- `leiCode.headquarter.country` shall equal `headquarterAddress.country`.
- `leiCode.legal.country` shall equal `legalAddress.country`.

## Natural person

To be defined in a future release.

<!--
TODO: JUNE 2022
which comes from ISO 25237: real human being as opposed to a legal person which may be a private or public organization

For natural person, the attributes are

| Version | Attribute                                      | Cardinality | Signed by | Comment                                           |
|---------|------------------------------------------------|-------|-----------------|---------------------------------------------------|
| 1.x     | `taxAddress[].`[`country`](https://schema.org/addressCountry) | 0..*  | State | location in [ISO 3166-1](https://www.iso.org/iso-3166-country-codes.html) alpha2, alpha-3 or numeric format. |
| 1.x     | `livingAddress[].`[`country`](https://schema.org/addressCountry) | 0..*  | State | Physical location in [ISO 3166-1](https://www.iso.org/iso-3166-country-codes.html) alpha2, alpha-3 or numeric format. |

-->

<!--
## Provider

A Provider aggregates with Participant.

| Version | Attribute          | Card. | Comment                                           |
|---------|--------------------|-------|---------------------------------------------------|
| x.x     | `to_be_defined`    | 1     | to be defined | mandatory public information      |

-->
