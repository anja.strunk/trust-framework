# Trust anchors

For the compliance, Trust anchors are Gaia-X endorsed entities responsible to manage certificate to sign claims.

To be compliant with the Gaia-X Trust Framework, all keypairs used to sign claims must have at least one of the Trust Anchor in their certificate chain.

At any point in time, the list of valid Trust Anchors is stored in the Gaia-X Registry.

# List of defined trust anchors

| Name     | Defined as |
|----------|------------|
| State    | The Trust Service Providers (TSP) must be a state validated identity issuer. <br/> - For `participant`, if the `legalAddress.country` is in EEA, the TSP must be **eiDAS** compliant. <br /> - Until end of 2022 Q1, to ease the onboarding and adoption this framework **DV SSL** can also be used. <br/> - **Gaia-X** association is also a valid TSP for Gaia-X association members. |
| eiDAS    | Issuers of Qualified Certificate for Electronic Signature as defined in eIDAS [Regulation (EU) No 910/2014](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2014.257.01.0073.01.ENG) <br/> (homepage: <https://esignature.ec.europa.eu/efda/tl-browser/#/screen/home>) <br/> (machine: <https://ec.europa.eu/tools/lotl/eu-lotl.xml>) |
| DV SSL   | Domain Validated (DV) Secure Sockets Layer (SSL) certificate issuers are considered to be temporarily valid Trust Service Providers. <br> (homepage: <https://wiki.mozilla.org/CA/Included_Certificates>) <br/> (machine: <https://ccadb-public.secure.force.com/mozilla/IncludedCACertificateReportPEMCSV>) |
| Gaia-X   | *To be defined after 2022Q1.* |
| EDPB CoC | List of Code of Conduct approved by the [EDBP](https://edpb.europa.eu/edpb_en) <br> (homepage: <https://edpb.europa.eu/our-work-tools/documents/our-documents_fr?f%5B0%5D=all_publication_type%3A61&f%5B1%5D=all_topics%3A125>) |
| gleif    | List of registered LEI issuers. <br> (homepage: <https://www.gleif.org/en/about-lei/get-an-lei-find-lei-issuing-organizations>) <br> (machine: <https://api.gleif.org/api/v1/registration-authorities>)|

<!--
| Gaia-X   | As a Gaia-X association member priviledge, Gaia-X association can issue Gaia-X Compliant signed claims for organization with a valid membership. |
-->

<!--
| EV SSL   | Starting from 2022 Q3, DV SSL will be replaced by Extented Validated (EV) Secure Sockets Layer (SSL) certificate issuers. |
-->
