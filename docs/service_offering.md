# Service Offering

While a more complete description of service offerings taxonomy is defined by the Service Characteristics Working Group and will be integrated inside that document, some services must already be specified to enable building a common governance across ecosystems.

```mermaid
flowchart BT
    so[Service Offering]
    dataset[Data management]
    dataset --> so

    dec[Data Exchange Connector]
    dec --> dataset

    wallet[Verifiable Credential Wallet]
    wallet --> dataset

    network[Network]
    network --> so

    inter[Interconnection]
    inter --> network
``` 

## Data exchange connector service


A `data connector` offering, such as and not limited to, a service bus, a messaging or an event streaming platform, must fulfil those requirements:

<!-- requirements from the DSBC position paper https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf -->

| Version | Attribute               | Card. | Signed by | Comment                                         |
|---------|-------------------------|-------|-----------|-------------------------------------------------|
| 1.0     | `physicalResource[]`   | 1..*  | State     | a list of `resource` with information of where is the data located. |
| 1.0     | `virtualResource[]`    | 1..*  | State     | a list of `resource` with information of who owns the data. |
| 1.0     | `policies[]`            | 1..*  | State     | a list of `policy` expressed using a DSL used by the data connector policy engine leveraging Gaia-X Self-descriptions as both data inputs and policy inputs |


## Verifiable Credential Wallet

A `wallet` enables to store, manage, present Verifiable Credentials:
<!-- requirements from the DSBC position paper https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf -->

| Version | Attribute                               | Card. | Signed by | Comment                                         |
|---------|-----------------------------------------|-------|-----------|-------------------------------------------------|
| 1.0     | `verifiableCredentialExportFormat[]` | 1..*  | State     | a list of machine readable format used to export verifiable credentials. |
| 1.0     | `privateKeyExportFormat[]`           | 1..*  | State     | a list of machine readable format used to export private keys. |

## Interconnection

An `interconnection` enables a mutual connection between two or more elements.

| Version | Attribute                               | Card. | Signed by | Comment                                         |
|---------|-----------------------------------------|-------|-----------|-------------------------------------------------|
| 1.x     | `location[].country`                    | 2..*  | State     | a list of physical location in ISO 3166-1 alpha2, alpha-3 or numeric format with at least the both ends of the connection. |
