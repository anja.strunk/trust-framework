# Examples

```mermaid
flowchart TB
    so{{Service Offering}}
    pr[/Physical Resource\]
    vr[\Virtual Resource\]
    ivr[\Instantiated Virtual Resource/]
    part[Participant]
```

## Generic LAMP offering

LAMP is an acronym for Linux, Apache, MySQL, PHP. It is a software stack consisting of the operating system, an HTTP server, a database management system and an interpreted programming language, and is used to set up a web server. 

```mermaid
flowchart TB
    wh -- providedBy --> prov1
    wh -. aggregationOf .-> lamp

    lamp -. aggregationOf .-> paas
    lamp -- copyrightOwnedBy --> softed1
    lamp -- tenantOwnedBy --> prov1

    lamp -- maintainedBy --> prov2

    server -- manufacturedBy --> manu1
    server -. aggregationOf .-> dc
    server -- onwedBy --> owner1
    server -- maintainedBy --> prov4

    dc -- manufacturedBy --> manu2
    dc -- onwedBy --> owner2
    dc -- maintainedBy --> prov5

    paas -. aggregationOf .-> server
    paas -- maintainedBy --> prov3
    paas -- copyrightOwnerBy --> softed2
    paas -- tenantOwnedBy --> prov2
    paas -. aggregationOf .-> conf

    conf -- copyrightOwnerBy --> prov3
    
    wh{{Web Hosting}}
    paas[\Platform as a Service/]
    prov1[Provider 1]

    lamp[\LAMP/]
    server[/Servers\]
    dc[/Datacenter\]
    conf[\Configuration\]

    softed1[Software Editor 1]
    prov2[Provider 2]

    softed2[Software Editor 2]
    prov3[Provider 3]

    manu1[Manufacturer 1]
    owner1[Owner 1]
    prov4[Provider 4]

    manu2[Manufacturer 2]
    owner2[Owner 2]
    prov5[Provider 5]
```

### LAMP offering using one software vendor

Example of a LAMP offering with one software vendor.  
This diagram can be used to illustrate how several "Trusted Cloud" offers are built.

```mermaid
flowchart TB
    wh -- providedBy --> prov1
    wh -. aggregationOf .-> lamp

    lamp -. aggregationOf .-> paas
    lamp -- tenantOwnedBy --> prov1

    lamp -- maintainedBy --> prov1

    server -. aggregationOf .-> dc
    server -- onwedBy --> softed1
    server -- maintainedBy --> softed1
    server -- manufacturedBy --> manu1

    dc -- onwedBy --> softed1
    dc -- manufacturedBy ---> manu2
    dc -- maintainedBy --> softed1

    paas -. aggregationOf ..-> server
    paas -. aggregationOf .-> conf
    paas -- maintainedBy --> prov1
    paas -- copyrightOwnerBy --> softed1
    paas -- tenantOwnedBy --> prov1

    conf -- copyrightOwnerBy --> softed1
    lamp -- copyrightOwnedBy --> softed1
    
    wh{{Web Hosting}}
    paas[\Platform as a Service/]
    prov1[National Provider]
    lamp[\LAMP/]
    server[/Servers\]
    dc[/Datacenter\]
    conf[\Configuration\]
    softed1[Non-National Software Editor]
    manu1[Manufacturer 1]
    manu2[Manufacturer 2]
```


## Simple Fortune teller

Example of a simple API endpoint returning a fortune from the BSD packet [fortune](https://en.wikipedia.org/wiki/Fortune_(Unix)). 

For the same service offering, 3 examples of service offering are detailled with 3 different transparency level:  
Trust_Index(**Service Offering 1 v1.0**) < Trust_Index(**Service Offering 1 v2.0**) < Trust_Index(**Service Offering 1 v3.0**)

### Fortune teller v1.0
<!--
    so{{Service Offering}}
    pr[/Physical Resource\]
    vr[\Virtual Resource\]
    ivr[\Instantiated Virtual Resource/]
-->

```mermaid
flowchart TB
    so1{{Fortune teller}}
    part1[Provider 1]
    so1 -- providedBy --> part1
```

**Service Offering**

```yaml
name: Fortune teller
description: API to randomly return a fortune
providedBy: url(provider1)
termsAndConditions:
  - https://some.url.for.terms.and.condition.example.com
```

**Provider 1**

```yaml
registrationNumber: FR5910.899103360
headquarterAddress:
  country: FR
legalAddress:
  country: FR
```

### Fortune teller v2.0

```mermaid
flowchart TB
    so1{{Fortune teller}}
    part1[Provider 1]
    vr1[\Software 1\]

    so1 -- providedBy --> part1
    so1 -- aggregationOf --> vr1
    vr1 -- copyrightOwnedBy --> part1
```

**Service Offering**

```yaml
name: Fortune teller
description: API to randomly return a fortune
providedBy: url(provider1)
aggregationOf:
  - url(software1)
termsAndConditions:
  - https://some.url.for.terms.and.condition.example.com
```

**Software 1**

```yaml
name: api software
copyrightOwnedBy:
  - url(provider1)
license:
  - EPL-2.0
```

### Fortune teller v3.0

```mermaid
flowchart TB
    so1{{Service Offering}}
    part1[Provider 1]
    part2[Participant 2]
    part3[Participant 3]
    vr1[\API/]
    vr2[\DataSet\]
    pr1[/Datacenter\]

    so1 -- providedBy --> part1
    so1 -- aggregationOf --> vr1 & vr2 & pr1
    vr1 -- copyrightOwnedBy --> part1
    vr1 -- tenantOwnedByBy --> part1
    vr1 -- maintainedBy --> part1

    vr2 -- copyrightOwnedBy --> part3

    pr1 -- maintainerBy --> part2
```
**Service Offering**

```yaml
name: Fortune teller
description: API to randomly return a fortune
providedBy: url(provider1)
aggregationOf:
  - url(software1)
  - url(dataset1)
  - url(datacenter1)
termsAndConditions:
  - https://some.url.for.terms.and.condition.example.com
policies:
  - type: opa
    content: |-
      package fortune
      allow = true {
        input.method = "GET"
      }
```

**API 1**

```yaml
name: api software
maintainedBy:
  - url(provider1)
tenantOwnedByBy:
  - url(provider1)
copyrightOwnedBy:
  - url(provider1)
license:
  - EPL-2.0
```

**Dataset 1**

```yaml
name: fortune dataset
copyrightOwnedBy:
  - name: The Regents of the University of California
    registrationNumber: C0008116
    headquarterAddress:
      state: CA
      country: USA
    legalAddress:
      state: CA
      country: USA
license:
  - BSD-3
  - https://metadata.ftp-master.debian.org/changelogs//main/f/fortune-mod/fortune-mod_1.99.1-7.1_copyright
```

**Participant 2**

```yaml
name: Cloud Service Provider
registrationNumber: FR5910.424761419
headquarterAddress:
  country: FR
legalAddress:
  country: FR
```

**Datacenter 1**

```yaml
name: datacenter
maintainedBy: url(participant2)
location:
  - country: FR
```
